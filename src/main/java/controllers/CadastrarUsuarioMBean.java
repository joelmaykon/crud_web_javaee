/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import dao.UsuarioDao;
import model.Usuario;

/**
 * Controller para cadastrar usuarios.
 *
 * @author joel
 */

@ManagedBean
@SessionScoped
public class CadastrarUsuarioMBean {
    
    private Usuario usuario;
    private List<Usuario> usuarios;
    /**
     * @return the usuario
     */
    

    public CadastrarUsuarioMBean() {
        usuario = new Usuario();
        usuarios = new ArrayList<Usuario>();
    }
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the usuarios
     */
    public List<Usuario> getUsuarios() {
       UsuarioDao usuarioDaoRecuperado = new UsuarioDao();
        usuarios = usuarioDaoRecuperado.buscarTodosUsuarios();
        return usuarios;
    }

    /**
     * @param usuarios the usuarios to set
     */
    public void setUsuarios(List<Usuario> usuarios) {
         
        this.usuarios = usuarios;
    }

    
    public String entrarCadastro() {
        return "/form_usuario.jsf";
    }
    public String voltar(){
        return "/index.jsf";
    }
    public String mostrar(){
        return "/lista.jsf";
    }
    public String cadastrar() {
        UsuarioDao usuarioDao = new UsuarioDao();
        usuarioDao.inserirUsuario(usuario);
        setUsuario(new Usuario());
        FacesMessage msg = new FacesMessage("Usuario cadastrado com sucesso!");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage("", msg);
        return "/form_usuario.jsf";
    }
}
