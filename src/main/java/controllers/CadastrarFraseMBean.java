/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import dao.FraseDao;
import model.Frase;
/**
 *
 * @author joel
 */
@ManagedBean
@SessionScoped
public class CadastrarFraseMBean {
   
   private Frase frase;
   private List<Frase>frases;
     public CadastrarFraseMBean() {
        frase = new Frase();
        frases = new ArrayList<Frase>();
    }
   public String entrarCadastro() {
        return "/form_frase.jsf";
    }
   public String voltar(){
       return "/index.jsf";
   }
   public String mostrar(){
       return "/lista.jsf";
   }
 public String cadastrar() {
        FraseDao fraseDao = new FraseDao();
        fraseDao.inserirFrase(frase);
        setFrase(new Frase());
        FacesMessage msg = new FacesMessage("Frase cadastrada com sucesso!");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage("", msg);
        return "/form_frase.jsf";
    }
    /**
     * @return the frase
     */
    public Frase getFrase() {
        return frase;
    }

    /**
     * @param frase the frase to set
     */
    public void setFrase(Frase frase) {
        this.frase = frase;
    }

    /**
     * @return the frases
     */
    public List<Frase> getFrases() {
        FraseDao fraseDao = new FraseDao();
        frases = fraseDao.buscarTodasFrases();
        return frases;
    }

    /**
     * @param frases the frases to set
     */
    public void setFrases(List<Frase> frases) {
        this.frases = frases;
    }
   
}
