/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Frase;
/**
 *
 * @author joel
 */
public class FraseDao {
     /**
     * Lista todas as frases cadastradas.
     *
     * @return
     */
    public List<Frase> buscarTodasFrases() {
        List<Frase> resultado = new ArrayList<Frase>();
        Connection con = GerenciadorConexao.getConexao();
        String sql = "select * from frase";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Frase frase = new Frase();
                frase.setNome(rs.getString("nome"));

                resultado.add(frase);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultado;
    }

    /**
     * Método para realizar a inserção de uma frase no BD.
     *
     * @param usuario
     */
    public void inserirFrase(Frase frase) {
        Connection con = GerenciadorConexao.getConexao();
        String sql = "insert into frase values (?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, frase.getNome());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
