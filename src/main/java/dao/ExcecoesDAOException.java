/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author joelmaykon
 */
public class ExcecoesDAOException extends Exception{

    public ExcecoesDAOException(){
        
    }
    public ExcecoesDAOException(String message) {
        super(message);
    }
    public  ExcecoesDAOException(Throwable message){
        super(message);
    }
    public ExcecoesDAOException(String message, Throwable message2){
        super(message,message2);
    }
    
}
