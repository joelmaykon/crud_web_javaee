/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author joel
 */
public class GerenciadorConexao {

    private static Connection conexao;

    /**
     * Método estático para obtenção de conexão. * @return
     */
    public static Connection getConexao() {
        if (conexao == null) {
            String username = "postgres";
            String password = "root";
            String url = "jdbc:postgresql://localhost:5432/postgres";
            try {
                Class.forName("org.postgresql.Driver");
                conexao = DriverManager.getConnection(url, username, password);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return conexao;
    }
}
