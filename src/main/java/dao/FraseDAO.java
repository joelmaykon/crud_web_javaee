/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Usuario;
import util.DataConnect;

/**
 *
 * @author joelmaykon
 */
public class FraseDAO {

    private Connection conn;

    public FraseDAO() throws ExcecoesDAOException {
        this.conn = DataConnect.getConnection();
    }

    public void atualizarFrase(Usuario usuario) throws ExcecoesDAOException {
        PreparedStatement ps = null;
        Connection conn = null;
        if (usuario == null) {
            throw new ExcecoesDAOException("O valor passado não pode ser nulo.");
        }

        try {
            String SQL = "UPDATE frase SET frase=? " + "WHERE nome_usuario=?;";
            conn = this.conn;
            ps = conn.prepareStatement(SQL);
            ps.setString(1, usuario.getFrase_usuario());
            ps.setString(2, usuario.getNome_usuario());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new ExcecoesDAOException("Erro ao atualizar dados:" + sqle);
        } finally {
            DataConnect.closeConnection(conn, ps);
        }
    }

    public void excluir(Usuario usuario) throws ExcecoesDAOException {
        PreparedStatement ps = null;
        Connection conn = null;
        if (usuario == null) {
            throw new ExcecoesDAOException("O valor passado não pode ser nulo.");
        }

        try {
            conn = this.conn;
            ps = conn.prepareStatement("DELETE FROM frase WHERE nome_usuario=?");
            ps.setString(1, usuario.getNome_usuario());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new ExcecoesDAOException("Erro ao excluir dados:" + sqle);
        } finally {
            DataConnect.closeConnection(conn, ps);
        }
    }

    public void salvar(Usuario usuario) throws ExcecoesDAOException {
        PreparedStatement ps = null;
        Connection conn = null;
        if (usuario == null) {
            throw new ExcecoesDAOException("O valor passado nao pode ser nulo.");
        }
        try {
            String SQL = "INSERT INTO frase(nome_usuario,frase)" + "VALUES(?,?)";
            conn = this.conn;
            ps = conn.prepareStatement(SQL);
            ps.setString(1, usuario.getNome_usuario());
            ps.setString(2, usuario.getFrase_usuario());
            ps.execute();
        } catch (SQLException sqle) {
            throw new ExcecoesDAOException("Erro ao inserir dados" + sqle);

        } finally {
            DataConnect.closeConnection(conn, ps);
        }
    }

    public List todasFrases() throws ExcecoesDAOException {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = this.conn;
            ps = conn.prepareStatement("SELECT * FROM frase");
            rs = ps.executeQuery();
            List<Usuario> list = new ArrayList<>();
            while (rs.next()) {
                String nome_usuario = rs.getString(1);
                String frase_usuario = rs.getString(2);   
                list.add(new Usuario(nome_usuario,frase_usuario));
            }
            return list;
        } catch (SQLException sqle) {
            throw new ExcecoesDAOException(sqle);
        } finally {
            DataConnect.closeConnection(conn, ps, rs);
        }
    }

    public Usuario procurarUsuario(String nome_usuario) throws ExcecoesDAOException {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = this.conn;
            ps = conn.prepareStatement("SELECT * FROM frase  WHERE nome_usuario = ?;");
            ps.setString(1, nome_usuario);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new ExcecoesDAOException("Não foi encontrado nenhum registro com esse nome ou data");
            }

            String nome = rs.getString(1);
            String frase = rs.getString(2);
            return new Usuario(nome, frase);
        } catch (SQLException sqle) {
            throw new ExcecoesDAOException(sqle);
        } finally {
            DataConnect.close(conn, ps, rs);
        }
    }

}
