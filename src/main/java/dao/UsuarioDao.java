/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Usuario;

/**
 *
 * @author joel
 */
public class UsuarioDao {

    /**
     * Lista todos os usuarios cadastrados.
     *
     * @return
     */
    public List<Usuario> buscarTodosUsuarios() {
        List<Usuario> resultado = new ArrayList<Usuario>();
        Connection con = GerenciadorConexao.getConexao();
        String sql = "select * from usuario";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Usuario usuario = new Usuario();
                usuario.setNome(rs.getString("nome"));
                usuario.setFrase(rs.getString("frase"));
                resultado.add(usuario);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultado;
    }

    /**
     * Método para realizar a inserção de um usuario no BD.
     *
     * @param usuario
     */
    public void inserirUsuario(Usuario usuario) {
        Connection con = GerenciadorConexao.getConexao();
        String sql = "insert into usuario values (?,?)";
        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, usuario.getNome());
            ps.setString(2, usuario.getFrase());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
