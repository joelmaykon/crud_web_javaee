/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import dao.ExcecoesDAOException;
import dao.FraseDAO;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import model.Usuario;

/**
 *
 * @author joelmaykon
 */
@ManagedBean
@SessionScoped
public class UsuarioMBean {

  private static final long serialVersionUID = 455659950717243338L;
  private Usuario usuario = new Usuario();
  Usuario novo_user = new Usuario();
  private FraseDAO fraseDAO = new FraseDAO();
  private List<Usuario> frases = new ArrayList<Usuario>();

  public UsuarioMBean() throws ExcecoesDAOException {
    frases = fraseDAO.todasFrases();
  }
  public void trocarFrase() throws ExcecoesDAOException{
     fraseDAO.atualizarFrase(usuario);
  }
  public void excluirFrase() throws ExcecoesDAOException{
    fraseDAO.excluir(usuario);
  }
  public void salvarFrase() throws ExcecoesDAOException{
    fraseDAO.salvar(usuario);
  }
  public Usuario buscarUsuario() throws ExcecoesDAOException{    
    novo_user = fraseDAO.procurarUsuario(novo_user.getNome_usuario());
    return novo_user;
  }
  
  public Usuario getUsuario() {
    return usuario;
  }

  public void setUsuario(Usuario usuario) {
    this.usuario = usuario;
  }

  public List<Usuario> getFrases() {
    return frases;
  }

  public void setEventos(List<Usuario> eventos) {
    this.frases = frases;
  }

  public Usuario getNullEvento() {
    return new Usuario();
  }
}
